$(document).ready(function() {

  $('.solutions .solutions-slider').on('init reInit afterChange breakpoint', function(event, slick, currentSlide, nextSlide){
    var n = $('.solutions .solutions-slider .slick-active').last().index() + 1;
    $('.solutions .slider-nav__count-current').html(n);
    $('.solutions .slider-nav__count-total').text(slick.slideCount);
  });

  $('.press .press-slider').on('init reInit afterChange breakpoint', function(event, slick, currentSlide, nextSlide){
    var n = $('.press .press-slider .slick-active').last().index() + 1;
    $('.press .slider-nav__count-current').text(n);
    $('.press .slider-nav__count-total').text(slick.slideCount);
  });

  $('.news .news-list').on('init reInit afterChange breakpoint', function(event, slick, currentSlide, nextSlide){
    var n = $('.news .news-list .slick-active').last().index() + 1;
    $('.news .slider-nav__count-current').text(n);
    $('.news .slider-nav__count-total').text(slick.slideCount);
  });

  $('.solutions .solutions-slider').slick({
      slidesToShow: 6,
      slidesToScroll: 6,
      arrows: true,
      infinite: false,
      prevArrow: '.solutions .slider-nav__arrows-prev',
      nextArrow: '.solutions .slider-nav__arrows-next',
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 4,
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
          }
        },
      ]
  });
  

  $('.press .press-slider').slick({
    slidesToShow: 4,
    slidesToScroll: 4,
    arrows: true,
    infinite: false,
    prevArrow: '.press .slider-nav__arrows-prev',
    nextArrow: '.press .slider-nav__arrows-next',
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1.3,
          slidesToScroll: 1,
        }
      },
    ]
  });

  $('.news .news-list').slick({
    slidesToShow: 1.3,
    slidesToScroll: 1,
    arrows: true,
    infinite: false,
    mobileFirst: true,
    prevArrow: '.news .slider-nav__arrows-prev',
    nextArrow: '.news .slider-nav__arrows-next',
    responsive: [
      {
        breakpoint: 768,
          settings: {
            slidesToShow: 1.3,
            slidesToScroll: 1,
          }
      },
      {
        breakpoint: 960,
          settings: "unslick"
      }
    ]
  });


  $('.projects__logos .projects__logos-item').on('click', function(e){
    e.preventDefault();
    var n = $(this).index();
    $('.projects__description .projects__description-item').hide();
    $('.projects__description .projects__description-item').eq(n).fadeIn(500);
  });

  $('.burger-wrapper').on('click', function(e){
    $('.menu-mobile').toggleClass('menu-mobile_active');
    $('body').toggleClass('menu-active');
  });


});

$(window).on('resize orientationchange', function() {
  $('.news .news-list').slick('resize');
});